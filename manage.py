from flask_migrate import MigrateCommand
from flask_script import Manager,Server
import os
from app import create_app

#hostenv = os.environ.get('HOSTENV') or 'default'
app = create_app()
manager = Manager(app)
manager.add_command('db',MigrateCommand)
manager.add_command('runserver',Server(host="127.0.0.1", port=5020))

if __name__ == '__main__':
    manager.run()

