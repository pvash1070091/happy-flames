from flask import Flask,request
from flask_migrate import Migrate


from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from authlib.integrations.flask_client import OAuth


db = SQLAlchemy()
ma = Marshmallow()


client_id = "698156182446-s3dd19il09o5k66hih5bofcrf85qgap4.apps.googleusercontent.com"
client_secret = "W-aErOV4_wK8rihgEY2OZtM2"
authorization_base_url = 'https://accounts.google.com/o/oauth2/v2/auth'
token_url = 'https://www.googleapis.com/oauth2/v4/token'


def create_app():
    app = Flask(__name__)
    from app.config import Config, ConfigDb
    app.config.from_object(Config)
    app.config.from_object(ConfigDb)
    db.init_app(app)
    ma.init_app(app)
    oauth = OAuth(app)
    migrate = Migrate(app, db)

    from app.login import views
    from app.login import models

    @app.route('/',methods=['GET'])
    def abc():

        return views.user()

    @app.route('/signup', methods=['POST'])
    def signup():
        data = request.json
        print("asd", data)
        re = views.postUser(data)
        return re

    @app.route('/login',methods=['POST'])
    def login():
        data=request.json
        return views.login(data)


    return app



