import os
from app.information import *

class Config(object):
    SECRET_KEY = "abcd"
    DEBUG = True
    CSRF_ENABLED = True

class ConfigDb(Config):
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://'+DB_USER+":"+DB_PASSWORD+'@'+DB_INSTANCE+'/'+DB_DATABASE
    SQLALCHEMY_TRACK_MODIFICATIONS = False