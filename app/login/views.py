from app import create_app, db
from flask import jsonify
import os
from app.login.models import User, UserSchema


app=create_app()

def user():

    user = User.query.all()
    if user is None:
        return jsonify({"response": "failure", "error": "No Users"}), 422
    else:
        response={
            'status_code':200,
            'result': UserSchema().dump(user,many=True)
        }
    print("working")
    return jsonify(response)

def postUser(data):
    print(data['email'])
    if 'email' not in data:
        return jsonify({"response": "failure", "error": "Email Missing"}), 422

    if 'mobile' not in data:
        return jsonify({"response": "failure", "error": "Mobile Missing"}), 422

    if 'password' not in data:
        return jsonify({"response": "failure", "error": "Password Missing"}), 422
    existing_user = User.query.filter_by(email=data['email']).first()
    if existing_user:
        return jsonify({"response": "failure", "error": "User Already exists"}), 422
    if existing_user.mobile == data['mobile']:
        return jsonify({"response": "failure", "error": "Mobile number exists"}), 422

    userPost = User(email = data['email'],mobile=data['mobile'],password=data['password'],display_name=data['display_name'])
    db.session.add(userPost)
    db.session.commit()
    response = {
        'message': 'new user registered'
    }
    return jsonify(response), 202

def login(data):
    if 'email' not in data:
        return jsonify({"response": "failure", "error": "Email Missing"}), 422
    if 'password' not in data:
        return jsonify({"response": "failure", "error": "Password Missing"}), 422
    else:
        user = User.query.filter_by(email = data['email']).first()
        if not user:
            return jsonify({"response": "failure", "error": "No such user exists"}), 422
        else:
            if not user.check_password(data['password']):
                return jsonify({"response": "failure", "error": "Incorrect password"}), 422
    resp = jsonify(response='success', email=user.email, mobile=user.mobile,
                   id=user.id, name=user.display_name)
    return resp,200










