
from werkzeug.security import generate_password_hash, check_password_hash

from app import db,ma


class User(db.Model):
    __tabelname__ = 'User'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    email = db.Column(db.String(128), unique=True,nullable=False)
    mobile = db.Column(db.String(10), unique=True,nullable=False)
    password = db.Column(db.String(128),nullable=False)
    display_name = db.Column(db.String(128),nullable=False)
    username = db.Column(db.String(128), unique=True)
    google_sub = db.Column(db.String(256))

    def __init__(self,password=None,email=None,mobile=None,display_name=None,username=None):
        self.email=email
        self.mobile=mobile
        self.display_name = display_name
        self.username = username
        self.set_password(password)

    def set_password(self, password):
        self.password = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password, password)

class UserSchema(ma.Schema):
    class Meta:
        fields = ('id', 'email','name','mobile')